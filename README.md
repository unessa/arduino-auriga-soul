# Arduino auriga SOUL

The soul of auriga robot.

Command center repository can be found from here https://bitbucket.org/unessa/arduino-auriga-command-center

# platform.io

Install platform.io
```
brew install platformio
```

initialize auriga board
```
platformio init --board=megaatmega2560
```

```
platformio run
```

```
platformio run --target upload
```
