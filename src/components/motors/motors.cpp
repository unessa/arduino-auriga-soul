#include <MePort.h>
#include "motors.h"

#define CLAMP(x, min, max) x < min ? min : x > max ? max : x

MeEncoderOnBoard leftMotor(SLOT_2);
MeEncoderOnBoard rightMotor(SLOT_1);

AurigaSoulMotors::AurigaSoulMotors()
  : leftMotorRpm(0.f), rightMotorRpm(0.f)
{
  attachInterrupt(leftMotor.getIntNum(), AurigaSoulMotors::isr_process_encoder1, RISING);
  attachInterrupt(rightMotor.getIntNum(), AurigaSoulMotors::isr_process_encoder2, RISING);

  //Set PWM 8KHz
  TCCR1A = _BV(WGM10);
  TCCR1B = _BV(CS11) | _BV(WGM12);

  TCCR2A = _BV(WGM21) | _BV(WGM20);
  TCCR2B = _BV(CS21);

  leftMotor.setPulse(7);
  leftMotor.setRatio(26.9);
  leftMotor.setPosPid(1.8,0,1.2);
  leftMotor.setSpeedPid(0.18,0,0);

  rightMotor.setPulse(7);
  rightMotor.setRatio(26.9);
  rightMotor.setPosPid(1.8,0,1.2);
  rightMotor.setSpeedPid(0.18,0,0);
}

AurigaSoulMotors::~AurigaSoulMotors() {
}

void AurigaSoulMotors::loop() {
  leftMotor.runSpeed(0.f);
  rightMotor.runSpeed(0.f);

  if (leftMotorRpm != 0.0f) {
    leftMotor.runSpeed(leftMotorRpm);
  }  

  if (rightMotorRpm != 0.0f) {
    rightMotor.runSpeed(rightMotorRpm);
  }

  leftMotor.loop();
  rightMotor.loop();
}

void AurigaSoulMotors::isr_process_encoder1()
{
      if(digitalRead(leftMotor.getPortB()) == 0){
            leftMotor.pulsePosMinus();
      } else {
            leftMotor.pulsePosPlus();
      }
}

void AurigaSoulMotors::isr_process_encoder2()
{
      if(digitalRead(rightMotor.getPortB()) == 0){
            rightMotor.pulsePosMinus();
      } else {
            rightMotor.pulsePosPlus();
      }
}

void AurigaSoulMotors::setLeftMotorRpm(int16_t rpm) {
  leftMotorRpm = CLAMP((float)rpm, 0.f, 255.f);
}

void AurigaSoulMotors::setRightMotorRpm(int16_t rpm) {
  rightMotorRpm = CLAMP((float)rpm, 0.f, 255.f);
}

float AurigaSoulMotors::getLeftMotorRpm() {
  return leftMotor.getCurrentSpeed();
}

float AurigaSoulMotors::getRightMotorRpm() {
  return rightMotor.getCurrentSpeed();
}
