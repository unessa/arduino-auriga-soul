#ifndef __AurigaSoul_motors_H_
#define __AurigaSoul_motors_H_

#include <MeEncoderOnBoard.h>

class AurigaSoulMotors {
  public:
    AurigaSoulMotors();
    ~AurigaSoulMotors();

    void loop();

    void setLeftMotorRpm(int16_t rpm);
    void setRightMotorRpm(int16_t rpm);

    float getLeftMotorRpm();
    float getRightMotorRpm();

  private:
    static void isr_process_encoder1();
    static void isr_process_encoder2();

    float leftMotorRpm;
    float rightMotorRpm;
};

#endif // __AurigaSoul_motors_H_
