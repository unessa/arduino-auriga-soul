#ifndef __AurigaSoul_eventTypes_H_
#define __AurigaSoul_eventTypes_H_

#include <stdint.h>

struct AurigaSoulEvent {
  // NOTE 06.03.2018 nviik - Temperature should be int16_t and conversion should be done in receiving end.
  //   That way it will consume 2 bytes less and auriga board doesn't have to convert it.
  float temperature;

  float leftMotorRpm;
  float rightMotorRpm;

  float compass_angle;
};

struct AurigaCommandCenterEvent {
  // How often auriga soul is sending data back to command center.
  uint16_t serialEventInterval;

  int16_t leftMotorRpm;
  int16_t rightMotorRpm;
};

#endif // __AurigaSoul_eventTypes_H_
