#ifndef __AurigaSoul_soul_H_
#define __AurigaSoul_soul_H_

#include <stdint.h>
#include <Wire.h>

#include "MeOnBoardTemp.h"
#include "MeCompass.h"
#include "components/motors/motors.h"

#define AURIGA_SOUL_BAUD_RATE 115200

class AurigaSoul {
  public:
    AurigaSoul();
    ~AurigaSoul();

    void setup();
    void loop();
    void serialEvent();

  private:
    void createAndPushAurigaSoulEvent();

    uint32_t soulEventTriggerInterval;
    MeOnBoardTemp temperatureReader;
    MeCompass compass;
    AurigaSoulMotors *motors;
};

#endif // __AurigaSoul_soul_H_
