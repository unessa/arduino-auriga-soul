
#include <MeAuriga.h>
#include "soul.h"

AurigaSoul *aurigaSoul = new AurigaSoul();


void serialEventRun() {
  aurigaSoul->serialEvent();
}

void setup() {
  aurigaSoul->setup();
}

void loop() {
  aurigaSoul->loop();
}
