#include <Arduino.h>
#include "event-types.h"
#include "soul.h"

#include "MeEncoderOnBoard.h"

AurigaSoul::AurigaSoul()
  : soulEventTriggerInterval(1000), temperatureReader(PORT_13),
  compass(PORT_6), motors(NULL)
{
}

AurigaSoul::~AurigaSoul() {
  delete motors;
}

void AurigaSoul::setup() {
  Serial.begin(AURIGA_SOUL_BAUD_RATE);

  motors = new AurigaSoulMotors();

  compass.begin();
  Serial.println(compass.testConnection() ?
    "compass connection created" : "compass connection failed");
}

void AurigaSoul::loop() {
  static uint32_t lastEventTime = 0;

  if(millis() - lastEventTime >= soulEventTriggerInterval) {
    lastEventTime += soulEventTriggerInterval;
    createAndPushAurigaSoulEvent();
  }

  if(motors) {
    motors->loop();
  }
}

void AurigaSoul::serialEvent() {
  int16_t availableBytes = Serial.available();

  const int16_t bufSize = sizeof(AurigaCommandCenterEvent);

  if(availableBytes == bufSize) {
    uint8_t buf[bufSize];
    Serial.readBytes(buf, bufSize);

    AurigaCommandCenterEvent *event = (AurigaCommandCenterEvent *)buf;
    soulEventTriggerInterval = event->serialEventInterval;

    if(motors) {
      motors->setLeftMotorRpm(event->leftMotorRpm);
      motors->setRightMotorRpm(event->rightMotorRpm);
    }
  }
}

void AurigaSoul::createAndPushAurigaSoulEvent() {
  float temperature = temperatureReader.readValue();

  AurigaSoulEvent event;
  event.temperature = temperature;
  event.leftMotorRpm = motors ? motors->getLeftMotorRpm() : 0.0f;
  event.rightMotorRpm = motors ? motors->getRightMotorRpm() : 0.0f;
  event.compass_angle = (float)compass.getAngle();

  Serial.write((const uint8_t *)&event, sizeof(event));
}
